package com.rehcan.vocabulary;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class LearnActivity extends AppCompatActivity {

    private Button _buttonWord,
                    _buttonYes,
                    _buttonNo;
    private DBHelper dbHelper;
    private List<Word> currentWords;
    private int c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);
        _buttonWord = findViewById(R.id.buttonWord);
        _buttonWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _buttonWord.setText(currentWords.get(c).getSecondWord());
            }
        });
        _buttonYes = findViewById(R.id.buttonYes);
        _buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextWord();
            }
        });
        _buttonNo = findViewById(R.id.buttonNo);
        _buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextWord();
            }
        });
        dbHelper = new DBHelper(getApplicationContext());
        currentWords = dbHelper.getAllWords();
        c = -1;
        nextWord();
    }
    private void nextWord() {
        if(c <= currentWords.size()) {
            c++;
            _buttonWord.setText(currentWords.get(c).getFirstWord());
        } else {
            Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
        }
    }
}
package com.rehcan.vocabulary;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.dbHelper = new DBHelper(getApplicationContext());
        final EditText editTextWord = findViewById(R.id.editTextWord);
        final EditText editTextTranslation = findViewById(R.id.editTextTranslation);
        Button _addWordButton = findViewById(R.id.buttonAddWord);
        _addWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!editTextWord.getText().toString().isEmpty()
                    && !editTextTranslation.getText().toString().isEmpty()) {
                    dbHelper.insertWord(editTextWord.getText().toString(), editTextTranslation.getText().toString());
                }
            }
        });
        Button _learnButton = findViewById(R.id.buttonStartLearning);
        _learnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent iGroupActivity = new Intent(getApplicationContext(), LearnActivity.class);
                startActivity(iGroupActivity);
            }
        });
        initializeDB();
    }
    private void initializeDB() {
        dbHelper.clearVocabulary();
        dbHelper.insertWord("Hallo", "Hello");
        dbHelper.insertWord("Auto", "Car");
        dbHelper.insertWord("Haus", "House");
        dbHelper.insertWord("Erstellen", "Create");
    }
}
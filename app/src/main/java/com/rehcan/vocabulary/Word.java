package com.rehcan.vocabulary;

public class Word {

    private String firstWord, secondWord;
    private int value;

    public String getFirstWord() {
        return firstWord;
    }
    public void setFirstWord(String firstWord) {
        this.firstWord = firstWord;
    }
    public String getSecondWord() {
        return secondWord;
    }
    public void setSecondWord(String secondWord) {
        this.secondWord = secondWord;
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
}
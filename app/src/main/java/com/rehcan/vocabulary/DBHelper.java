package com.rehcan.vocabulary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "vocabulary.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE vokabel" +
                "(id INTEGER PRIMARY KEY, first TEXT, second TEXT, value INT)"
        );
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS vokabel");
        onCreate(sqLiteDatabase);
    }
    public boolean insertWord(String first, String sec) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("first", first);
        contentValues.put("second", sec);
        contentValues.put("value", 0);
        sqLiteDatabase.insert("vokabel", null, contentValues);
        return true;
    }
    public ArrayList<Word> getAllWords() {
        ArrayList<Word> words = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM vokabel", null);
        while(cursor.moveToNext()) {
            Word tempWord = new Word();
            tempWord.setFirstWord(cursor.getString(cursor.getColumnIndex("first")));
            tempWord.setSecondWord(cursor.getString(cursor.getColumnIndex("second")));
            tempWord.setValue(cursor.getInt(cursor.getColumnIndex("value")));
            words.add(tempWord);
        }
        cursor.close();
        return words;
    }
    public void clearVocabulary() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM vokabel");
    }
}